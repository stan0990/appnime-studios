# Appnime App
Instructions for running Appnime Studios App
After installing all the react native development environment we will need to perform the following steps to be able to run this app.
All project is in the branch develop of this repository, and there are two ways for running this project:

#1:

- Download Repository from bitbucket using *git clone git@bitbucket.org:stan0990/appnime-studios.git* or *git clone https://stan0990@bitbucket.org/stan0990/appnime-studios.git*
- Go to app's folder
- Go to Branch develop with *git checkout develop*
- Type *npm install* for installing modules and dependencies of project
- We need to link our dependencies using *react-native link*
- Type *react-native start* on console
- In another console you will run this application with *react-native run-android*

#2:
- There are an apk file in */apk/app-release.apk*, is a build for installing on your phone.


** This application just works with Android Operative System.